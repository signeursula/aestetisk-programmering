
let kliklyd;
let errorBillede;
let reklame1Billede;
let reklame2Billede;
let capture;
let knap;
let knapFarve;
let blitzAlpha = 0;
let blitzTæller = 0;
let klikTæller = 0;
let nul = 0;

function preload(){
 //Forbereder på lydimput
 soundFormats('mp3');
 kliklyd = loadSound('kliklyd.mp3');
 kliklyd.setVolume(0.3);
 }

function setup() {
 //Consol.loggen
 console.log("HELLO DATA WORLD!");
 
 //Rød knap der tager billede
 knap = createButton("O");
 knapFarve = 243;
 knap.position(670, 600);
 knap.size(45, 45);
 knap.style("background-color", "#ff0000");
 knap.style("color", "#ff0000");

 //Når musen holdes over knappen, ændrer den farve
 knap.mouseOver(knapSkifteFarve);
 knap.mouseOut(knapResetFarve);

 //Web cam capture
 capture = createCapture(VIDEO);
 capture.size(640, 480);
 capture.hide(); 

 //Kanvasset
 createCanvas(windowWidth, windowHeight);
 background(10);
 errorBillede = loadImage('error.png'); 
 
 //Upload af billeder
 reklame1Billede = loadImage('reklame1.png'); 
 reklame2Billede = loadImage('reklame2.png'); 

 //Klik på rød knap
 knap.mousePressed(klikPåKnappen);
}

function draw() {
 //Overskrift: SMART-PHOTOBOOTH
 fill(255);
 textSize(50);
 textStyle(BOLD);
 text('SMART-PHOTOBOOTH', 410, 75);

 //Blitz tælleren
 if(frameCount % 10 == 0){
 blitzTæller = blitzTæller + 1;}

 //Display af webcamet
 image(capture, 350, 100, 640, 480);

 //Blitz effekt
 fill(255, blitzAlpha);
 rect(350, 100, 640, 480);

 //Webcammets grå ramme
 fill(30);
 noStroke();
 rect(350, 100, 640, 40);
 rect(350, 100, 40, 480);
 rect(350, 580, 680, 80);
 rect(990, 100, 40, 520);

 //Blitzen aftager
 if (blitzTæller == 2){
 blitzAlpha = nul;}

 //Errors:
   //Error 1
   if (klikTæller == 1){
   push();
   fill(230);
   stroke(200);
   strokeWeight(4);
   rect(40, 300, 260, 60);
   image(errorBillede, 50, 310, 20, 20);
   fill(0);
   noStroke();
   textStyle(NORMAL);
   textSize(11);
   text('Error! Du kan da ikke tage et billede med det hår!', 80, 315, 200, 100);
   pop();}

     //Error 2
     if (klikTæller == 2){
     push();
     fill(230);
     stroke(200);
     strokeWeight(4);
     rect(60, 400, 260, 60);
     image(errorBillede, 70, 410, 20, 20);
     fill(0);
     noStroke();
     textStyle(NORMAL);
     textSize(11);
     text('Error! Smart-photobooth vurderer, at du burde tage billedet i bedre belysning', 100, 415, 200, 100);
     pop();}

       //Error 3
       if (klikTæller == 3){
       push();
       fill(230);
       stroke(200);
       strokeWeight(4);
       rect(60, 180, 260, 60);
       image(errorBillede, 70, 190, 20, 20);
       fill(0);
       noStroke();
       textStyle(NORMAL);
       textSize(11);
       text('Error! Har du overvejet at bruge lidt makeup?', 100, 195, 200, 100);
       pop();}

         //Error 4
         if (klikTæller == 4){
         push();
         fill(230);
         stroke(200);
         strokeWeight(4);
         rect(50, 540, 260, 60);
         image(errorBillede, 60, 550, 20, 20);
         fill(0);
         noStroke();
         textStyle(NORMAL);
         textSize(11);
         text('Error! Billedet kan ikke tages i denne tilstand. Prøv at revurdér din fremtoning', 90, 555, 200, 100);
         pop();}
        
           //Error 5 (reklame)
           if (klikTæller == 5){
           image(reklame1Billede, 20, 20, 250, 125);}
            
             //Error 6 (reklame)
             if (klikTæller == 6){
              image(reklame2Billede, 1100, 40, 150, 600);}
} 

function klikPåKnappen(){
 //Kniktælleren stiger når der klikkes
 klikTæller = klikTæller + 1;
 kliklyd.play();

 //Kameraets blitz
 blitzAlpha =+ 230;
 blitzTæller = nul;
 }

function knapSkifteFarve() {
 //Når man hover over knappen bliver den mørkerød
 knap.style("background-color", "#d10d0d");
 knap.style("color", "#d10d0d");
 }
 
function knapResetFarve() {
 //Når man ikke hover over knappen er den rød
 knap.style("background-color", "#ff0000");
 knap.style("color", "#ff0000");
 }