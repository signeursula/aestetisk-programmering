# MiniX4 - README

Link til MiniX4 [https://signeursula.gitlab.io/aestetisk-programmering/MiniX4/index.html]

Link til source code [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX4/smart-photobooth.js]

Jeg har lavet et program, som hedder Smart-photobooth. Ved første øjekast skal programmet ummidelbart ligne et typisk computer-kamera program, hvor man kan tage billeder af sig selv. Dette er lavet ved, at der er en boks i midten af kanvasset, hvori man kan se sig selv, hvis man har givet tilladelse til kamerafunktionen. Over boksen står der også "SMART-PHOTOBOOTH" således at brugeren allerede har en forforståelse om, at det er en photobooth som kan et eller andet smart. I dag kan man jo nærmest putte ordet "smart" foran alt teknologi for at få det til at lyde sejere. Smart-tv, smart-watches, smart termostat, osv... Ikke at det ikke er smart. Typisk reffererer "smart" også til et stykke teknologi som indeholder artificial intelligence. Selve mit program indeholder ikke rigtig artificial intelligence, men det har alligevel til formål at udstille AI. Artificial intelligence får netop al sin viden igennem dataindsamling, og dataindsamling er overalt og sker konstant -også ubemærket i vores dagligdag. Dette vil Smart-photobooth gerne kritisere.

Under videoboksen er der en rød knap. Denne røde knap bliver lidt mørkere, når man holder musen over den. Dette gjorde jeg for endnu mere at give en knap-effekt, og for at vise, at man bør trykke på den. Klikker du på den "tager den et billede". Den tager ikke rigtigt et billede, men der kommer en "klik" lyd, og kameraet bliver hurtigt hvidt, som hvis man havde taget et billede med blitz. Det er hér, at programmet bliver kritisk. Programmet lader dig ikke tage billedet, da den kommer med en pop-up meddelelse, hvor der står "Error! Du kan da ikke tage et billede med det hår!". Dette har jeg valgt, at den skal gøre for at synliggøre, at der sidder noget kunstig intelligens og kigger med, og bruger _dig_ som data, og sammenligner dig med andres data. Trykker du yderligere tre gange vil flere errors dukke op. Bagefter vil der begynde at komme reklamer (reklamer jeg selv har sat ind). Der kommer både reklamer for plastikkirurgi og teeth whiteming.

Programmet er en form for kritisk kunst. Det er meningen, at smart-photoboothen skal disse dig, og sætte nogle tanker igang. Er der normalt kunstlig intelligens der kigger med, når jeg tager billeder? Blandt andet har Meta adgang til din data fra Facebook -også det du ikke har delt. Bruges dine ikke-postede billeder som data? Sammenligner kunstig intelligens dig med andre og andre med dig? Dette er meget interresant at reflektere over. Og hvordan ser man rigtig ud? Det kan have konsekvenser kulturelt, da vi ser forskellige ud i verden, og der ikke er noget rigtigt facit.

**Men nu lidt om min kode!**

Først og fremmest har jeg skulle lave min knap, da hele programmet er bygget op om knappen. Dette gjorde jeg ved først at lave en variabel som hedder knap: `let knap;`. Ved at bruge `knap.mouseOver(knapSkifteFarve);` og `knap.mouseOut(knapResetFarve);` kunne jeg gøre, at knappen ændrede farve, når man hover musemarkøren over den.

Her kommer noget af det, som jeg havde lidt sværere ved. Min idé var, at ved at trykke på knappen, skulle der komme en kliklyd, skærmen skulle kortvarigt blive hvid, og der skulle komme nogle pop-ups. Først og fremmest skrev jeg: `knap.mousePressed(klikPåKnappen);`. Lyden var let nok at få til at virke. Det gjorde jeg ved: 

`function klikPåKnappen(){`

`kliklyd.play();`

Nu skulle jeg få skærmen til at blive hvid. Min første tanke var, at jeg skulle lave en funktion, som fik en hvid rektangel på størrelse med videoen til at opstå og så forsvinde igen. Dette kunne jeg simpelthen ikke greje, men så fik jeg idéen om, at jeg jo bare hele tiden kunne have den hvide blitz-rektangel til at være der, og så bare gøre den gennemsigtig. Derfor lavede jeg variablen: `let blitzAlpha = 0;`. Den er så sat til 0, da den som udgangspunkt skal starte med at være gennemsigtig.

`fill(255, blitzAlpha);`

`rect(350, 100, 640, 480);`

Når billedet så bliver taget, så plusses blitzAlphaen med 230, således at den hvide rektangel bliver synlig.

`function klikPåKnappen(){`

`blitzAlpha =+ 230;`

Men jeg skulle jo også få blitzen (den hvide rektangel) til at aftage igen. I starten tænkte jeg, at jeg kunne få frameCounten til at starte forfra, men det læste jeg mig til, at det ville crashe programmet, så det gjorde jeg ikke. I stedet lavede jeg en tæller, som løber parallelt med frameCounten. Denne variabel har jeg valgt at kalde: `let blitzTæller = 0;`. For at få blitzTælleren til at stige skrev jeg:

`if(frameCount % 10 == 0){` 

`blitzTæller = blitzTæller + 1;}`

Dette vil sige, at hver gang at frameCounten er på 10, tilføjes der 1 til blitzTælleren.

Jeg har så valgt, at når blitzTælleren rammer tallet 2, vil den sørge for, at den hvide rektangel (altså blitzen) bliver gennemsigtig igen:

`if (blitzTæller == 2){`

`blitzAlpha = nul;}` (nul er en variabel jeg lavede for lettere at kunne nulstille, da det ikke virkede, da jeg bare skrev 0)

**SÅ alt i alt** hvordan det her virker er:

`function klikPåKnappen(){`

`blitzAlpha =+ 230;` BlitzAlphaen sættes op.

`blitzTæller = nul;` BlitzTælleren resettes, og når den rammer tallet 2, vil BlitzAplhaen nulstilles.

Jeg ved ikke, om der er en smartere måde at gøre det på, men jeg er meget stolt over, at jeg fik det til at virke. Jeg har ikke gjort mig så meget i variabler før, så det her var lidt af en udfordring.

Selve pop-up errors'ne er lavet med `if statements`. Jeg måtte lave en ny variabel som talte antal klik. Det gjorde jeg ved: 

`let klikTæller = 0;`

og

`function klikPåKnappen(){`

`klikTæller = klikTæller + 1;`

Således kunne jeg specifikt tælle antal kliks. Pop-up errors'ne ser sådan her ud, og de er lavet i `push()` og `pop()` for ikke at forstyrre andre elementer. (Det virkede ikke, når jeg ikke skrev dem i push og pop).

`//Error 1`

`if (klikTæller == 1){`

`push();`

`fill(230);`

`stroke(200);`

`strokeWeight(4);`

`rect(40, 300, 260, 60);`

`image(errorBillede, 50, 310, 20, 20);`

`fill(0);`

`noStroke();`

`textStyle(NORMAL);`

`textSize(11);`

`text('Error! Du kan da ikke tage et billede med det hår!', 80, 315, 200, 100);`

`pop();}`

![](MiniX4/Smart-photobooth_screenshot.png)

