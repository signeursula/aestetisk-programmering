// Class fil til spiller 1 og deres bevaegelse inden for spillet
class Spiller1 {
    constructor() {
        this.posX       = 535;
        this.posY       = 640;
        this.speed      = 20;
        this.sizeX      = 25;
        this.sizeY      = 52;
        this.direction  = "højre";
    }

    show() {
        push();
        imageMode(CENTER);
        if (this.direction === "højre") {
            image(player1højreImg, this.posX, this.posY, this.sizeX, this.sizeY);
        } else if (this.direction === "venstre") {
            image(player1venstreImg, this.posX, this.posY, this.sizeX, this.sizeY);
        }
        pop();
    }

    moveLeft() {
        if (this.posX - this.speed >= 370) {
            this.posX -= this.speed;
            this.direction = "venstre";
        }
    }

    moveRight() {
        if (this.posX + this.speed <= 710) {
            this.posX += this.speed;
            this.direction = "højre";
        }
    }
}