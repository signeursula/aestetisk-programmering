# MiniX11 - README

♡ Gruppe: [Joan](https://gitlab.com/joan6375305/aestetisk-programmering/-/tree/main), [Marius](https://gitlab.com/elmose-gruppe/aestetisk-programmering/-/tree/main/), [Signe](https://gitlab.com/signeursula/aestetisk-programmering/-/tree/main/) ♡

Link til RUNME: [https://signeursula.gitlab.io/aestetisk-programmering/MiniX11/index.html]

Link til draft: [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX11/Draft.pdf]

Link til opgaven: [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX11/Eksamensprojektet.pdf]
