/* MILLIONÆR-SPILLET
MiniX 11 af Gruppe 8 
Signe Ursula, Joan Gunnarsdottir & Marius Elmose

Det her er koden til vores MiniX 11, vores eksamensprojekt til ÆP.
Vi har valgt at lave et spil, hvor man konkurrer mod hinanden
om at opbygge en virksomhed. I takt med at man opbygger den her
virksomhed, bliver man opmærksom på de konsekvenser det har 
for klimaet i den verden man laver sin virksomhed i.
*/

// Variabler for Kanvas og Overskrift
let kanvasBredde = 1470;
let kanvasHøjde = 725;
let overskriftImg;
let KanvasBaggrundImg;

// Variabler for Knapper
let pixelFont;
let knap1Img;
let knap2Img;
let vehImg;
let afdImg;

// Variabler for Spillerne
let player1;
let player1saldo = 0;
let player1højreImg;
let player1venstreImg;

let player2;
let player2saldo = 0;
let player2højreImg;
let player2venstreImg;

// Variabler for penge
let lillepenge1;
let lillepenge2;
let mellempenge1;
let mellempenge2;
let storpenge1;
let storpenge2;

let penge1Img;
let penge2Img;
let penge3Img;

// Variabler for opgradering
let player1opgradering = 400000;
let player2opgradering = 400000;

//Baggrundsbilleder
let baggrund1Img;
let baggrund2Img;
let baggrund3Img;
let baggrund4Img;
let baggrund5Img;
let baggrund6Img;
let baggrund7Img;
let baggrund8Img;

//Vindersider
let vinder1alpha = 0;
let vinder2alpha = 0;
let storplayer1Img;
let storplayer2Img;

//Spil-igen knap
let spilIgen;
let gameOver = false;

//Smadret verden
let smadretVerden1alpha = 0;
let smadretVerden2alpha = 0;

/////////////////////// Pre - Load //////////////////////////////////////////////////////////////////////////////////

// Vi bruger preload til at loade adskillelige billeder og en font
function preload() {
  player1højreImg   = loadImage("Billeder/player1højre.png");
  player1venstreImg = loadImage("Billeder/player1venstre.png");
  player2højreImg   = loadImage("Billeder/player2højre.png");
  player2venstreImg = loadImage("Billeder/player2venstre.png");

  storplayer1Img    = loadImage("Billeder/storplayer1.jpg");
  storplayer2Img    = loadImage("Billeder/storplayer2.jpg")

  baggrund1Img      = loadImage("Billeder/baggrund1.png");
  baggrund2Img      = loadImage("Billeder/baggrund2.png");
  baggrund3Img      = loadImage("Billeder/baggrund3.png");
  baggrund4Img      = loadImage("Billeder/baggrund4.png");
  baggrund5Img      = loadImage("Billeder/baggrund5.png");
  baggrund6Img      = loadImage("Billeder/baggrund6.png");
  baggrund7Img      = loadImage("Billeder/baggrund7.png");
  baggrund8Img      = loadImage("Billeder/baggrund8.png");

  overskriftImg     = loadImage("Billeder/overskrift.png");
  KanvasBaggrundImg = loadImage("Billeder/KanvasBaggrund.png");

  knap1Img          = loadImage("Billeder/knap1.png");
  knap2Img          = loadImage("Billeder/knap2.png");
  afdImg            = loadImage("Billeder/AFD.png")
  vehImg            = loadImage("Billeder/VEH.png")

  penge1Img         = loadImage("Billeder/penge1.png");
  penge2Img         = loadImage("Billeder/penge2.png");
  penge3Img         = loadImage("Billeder/penge3.png");

  pixelFont         = loadFont('Fonts/pixelFont.ttf');
}
/////////////////////////// Setup ///////////////////////////////////////////////////////////////////////////////////
/* 
I setup definerer vi kanvas efter specifikke værdier - ÆNDR 
*/
function setup() {
  createCanvas(kanvasBredde, kanvasHøjde);

  // Penge
  lillepenge1 = new LillePenge1();
  lillepenge2 = new LillePenge2();
  mellempenge1 = new MellemPenge1();
  mellempenge2 = new MellemPenge2();
  storpenge1 = new StorPenge1();
  storpenge2 = new StorPenge2();

  // Spillere
  player1 = new Spiller1();
  player2 = new Spiller2();

 // Spil-igen knap
 spilIgen = createButton("SPIL IGEN!");
 spilIgen.position(696, 540);
 spilIgen.size(180, 50);  
 spilIgen.style('font-size', '24px');
 spilIgen.style("font-family", "pixelFont");
 spilIgen.style("border", "4px solid #ffffff");
 spilIgen.style("background-color","#000000");
 spilIgen.style("color", "#ffffff");
 spilIgen.mousePressed(genstartSpil);

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function genstartSpil (){
  gameOver = false;
  vinder1alpha = 0;
  vinder2alpha = 0;
  player1opgradering = player1opgradering = 400000;
  player2opgradering = player2opgradering = 400000;
  player1saldo = player1saldo = 0;
  player2saldo = player2saldo = 0;
}

/////////////////////////// Draw ////////////////////////////////////////////////////////////////////////////////////
/* ... */
function draw() {
  //Baggrund
  image(KanvasBaggrundImg, 0, 0);
  
  strokeWeight(5);
  collisionCheck1();
  collisionCheck2();

/* Venstre side af spiller-skærmen  */
image(baggrund1Img, 350, 150, 370, 540);
player1.show();
 
// Knapper på venstre side
image(knap1Img, 100, 150, 230, 100);
push();
fill(0);
textSize(24);
textFont('pixelFont');
text("SALDO:",140,195);
textSize(30);
text((player1saldo)+("$"), 140, 225);
pop();

image(knap2Img,100,280,230,160);
push();
fill(0);
textSize(24);
textFont('pixelFont');
text("OPGRADER DIN",130,330);
text("VIRKSOMHED:",140,355);
pop();

/* Højre side af spiller-skærmen */
image(baggrund1Img, 760, 150, 370, 540);
player2.show();

//knapper på højre side
image(knap1Img, 1150, 150, 230, 100);
push();
fill(0);
textSize(24);
textFont('pixelFont');
text("SALDO:",1190,195);
textSize(30);
text((player2saldo)+("$"), 1190, 225);
pop();

image(knap2Img, 1150, 280, 230, 160);
push();
fill(0);
textSize(24);
textFont('pixelFont');
text("OPGRADER DIN",1180,330);
text("VIRKSOMHED:",1195,355);
pop();

 //AFD (A, F og D) og VEH (venstre, enter og højre) knapper
  image(afdImg, 100, 490, 230, 180)
  image(vehImg, 1150, 490, 230, 180)

 //Overskrift
 image(overskriftImg, 320, 45, 830, 80);

 // Viser pengene fra Class'
 lillepenge1.move(); 
 lillepenge1.show();
 lillepenge2.move(); 
 lillepenge2.show(); 

 mellempenge1.move(); 
 mellempenge1.show(); 
 mellempenge2.move(); 
 mellempenge2.show(); 

 storpenge1.move(); 
 storpenge1.show(); 
 storpenge2.move(); 
 storpenge2.show(); 

 //Visning af opgraderingspris
 fill(0);
 textSize(30);
 textFont('pixelFont');
 text((player1opgradering)+("$"), 140, 400);
 text((player2opgradering)+("$"), 1195, 400);

 //Når spilleren får råd til at opgradere
 if (player1saldo >= player1opgradering && keyCode === 70){
  player1saldo = player1saldo - player1opgradering;
  player1opgradering = player1opgradering + 100000;
 }
 
 if (player2saldo >= player2opgradering && keyCode === ENTER){
  player2saldo = player2saldo - player2opgradering;
  player2opgradering = player2opgradering + 100000;
 }

 //Baggrunde skifter for player 1
 if(player1opgradering === 500000){
  image(baggrund2Img, 350, 150, 370, 540);
  player1.show();
  lillepenge1.show();
  mellempenge1.show(); 
  storpenge1.show(); 
 }
 if(player1opgradering === 600000){
  image(baggrund3Img, 350, 150, 370, 540);
  player1.show();
  lillepenge1.show();
  mellempenge1.show(); 
  storpenge1.show(); 
 }
 if(player1opgradering === 700000){
  image(baggrund4Img, 350, 150, 370, 540);
  player1.show();
  lillepenge1.show();
  mellempenge1.show(); 
  storpenge1.show(); 
 }
 if(player1opgradering === 800000){
  image(baggrund5Img, 350, 150, 370, 540);
  player1.show();
  lillepenge1.show();
  mellempenge1.show(); 
  storpenge1.show(); 
 }
 if(player1opgradering === 900000){
  image(baggrund6Img, 350, 150, 370, 540);
  player1.show();
  lillepenge1.show();
  mellempenge1.show(); 
  storpenge1.show(); 
 }
 if(player1opgradering === 1000000){
  image(baggrund7Img, 350, 150, 370, 540);
  player1.show();
  lillepenge1.show();
  mellempenge1.show(); 
  storpenge1.show(); 
 }
 
//Baggrunde skifter for player 2
if(player2opgradering === 500000){
  image(baggrund2Img, 760, 150, 370, 540);
  player2.show();
  lillepenge2.show();
  mellempenge2.show(); 
  storpenge2.show(); 
 }
 if(player2opgradering === 600000){
  image(baggrund3Img, 760, 150, 370, 540);
  player2.show();
  lillepenge2.show();
  mellempenge2.show(); 
  storpenge2.show(); 
 }
 if(player2opgradering === 700000){
  image(baggrund4Img, 760, 150, 370, 540);
  player2.show();
  lillepenge2.show();
  mellempenge2.show(); 
  storpenge2.show(); 
 }
 if(player2opgradering === 800000){
  image(baggrund5Img, 760, 150, 370, 540);
  player2.show();
  lillepenge2.show();
  mellempenge2.show(); 
  storpenge2.show(); 
 }
 if(player2opgradering === 900000){
  image(baggrund6Img, 760, 150, 370, 540);
  player2.show();
  lillepenge2.show();
  mellempenge2.show(); 
  storpenge2.show(); 
 }
 if(player2opgradering === 1000000){
  image(baggrund7Img, 760, 150, 370, 540);
  player2.show();
  lillepenge2.show();
  mellempenge2.show(); 
  storpenge2.show(); 
 }

 //////////////////////////////////vinder-sider//////////////////////////////////////////////////////////////////////

 //Player 1 vinder-side
 push();
   push();
     tint(255, smadretVerden1alpha);
     image(baggrund8Img, 350, 150, 370, 540);
     image(knap1Img, 100, 150, 230, 100);
     fill(0, smadretVerden1alpha)
     textSize(24);
     textFont('pixelFont');
     text("SALDO:",140,195);
     textSize(30);
     text("1000000 $", 140, 225);
     image(knap2Img,100,280,230,160);
     fill(0, smadretVerden1alpha);
     textSize(24);
     textFont('pixelFont');
     text("TILLYKKE",140,340);
     text("MED DIN",140,365);
     text("VIRKSOMHED",140,390);
   pop();
 
 fill(0, vinder1alpha);
 rect(0, 0, 1470, 725);
 fill(255, vinder1alpha);
 textSize(70);
 text("SPILLER 1", 540, 162);
 textSize(100);
 text("HAR VUNDET!!!", 400, 280);
 textSize(26);
 text("TILLYKKE! DIN VIRKSOMHED", 700, 392);
 text("HAR HAFT FANTASTISK VÆKST", 700, 430);
 text("OG DU ER NU BLEVET MILLIONÆR!", 700, 468);
 text("GODT GÅET!", 700, 506);
 tint(255, vinder1alpha);
 image(storplayer1Img, 400, 310, 245, 358);
 pop();

 //Player 2 vinder-side
  push();
  
   push();
     tint(255, smadretVerden2alpha);
     image(baggrund8Img, 760, 150, 370, 540);
     image(knap1Img, 1150, 150, 230, 100);
     fill(0, smadretVerden2alpha)
     textSize(24);
     textFont('pixelFont');
     text("SALDO:",1190,195);
     textSize(30);
     text("1000000 $", 1190, 225);
     image(knap2Img, 1150, 280, 230, 160);
     fill(0, smadretVerden2alpha);
     textSize(24);
     textFont('pixelFont');
     text("TILLYKKE",1190,340);
     text("MED DIN",1190,365);
     text("VIRKSOMHED!",1190,390);
   pop();

  fill(0, vinder2alpha);
  rect(0, 0, 1470, 725);
  fill(255, vinder2alpha);
  textSize(70);
  text("SPILLER 2", 540, 162);
  textSize(100);
  text("HAR VUNDET!!!", 400, 280);
  textSize(26);
  text("TILLYKKE! DIN VIRKSOMHED", 700, 392);
  text("HAR HAFT FANTASTISK VÆKST", 700, 430);
  text("OG DU ER NU BLEVET MILLIONÆR!", 700, 468);
  text("GODT GÅET!", 700, 506);
  tint(255, vinder2alpha);
  image(storplayer2Img, 440, 310, 164, 346);
  pop();

 //Hvis player 1 vinder
 if(player1opgradering === 1100000){
  vinder1alpha = vinder1alpha + 255;
  player1opgradering = player1opgradering + 1;
  smadretVerden1alpha = smadretVerden1alpha + 255;
  gameOver = true;
}

 //Hvis player 2 vinder
 if(player2opgradering === 1100000){
 vinder2alpha = vinder2alpha + 255;
 player2opgradering = player2opgradering + 1;
 smadretVerden2alpha = smadretVerden2alpha + 255;
 gameOver = true;}

   // Vis spil-igen knap
   if (vinder1alpha >= 255 || vinder2alpha >= 255) {
    spilIgen.show();
  } else { 
    spilIgen.hide(); }

}

////////////////////////// KeyPressed ///////////////////////////////////////////////////////////////////////////////
/* 
Her definerer vi hvordan spillerne rykker sig på canvas'et
Spiller 1 rykker sig efter A-knappen, og D-knappen
Spiller 2 rykker sig efter venstre-pil og højre pil
*/
function keyPressed() {
  if (keyCode === 65) { //A
    player1.moveLeft();
  } else if (keyCode === 68) { //D
    player1.moveRight();
  } else if (keyCode === LEFT_ARROW) {
    player2.moveLeft();
  } else if (keyCode === RIGHT_ARROW) {
    player2.moveRight();
  }}
  

////////////////////////// CollisionCheck ///////////////////////////////////////////////////////////////////////////
/*...*/
function collisionCheck1() {
 //Sammenstødet mellem player 1 og LillePenge
 let distance1l;
 distance1l = dist(player1.posX, player1.posY, lillepenge1.posX, lillepenge1.posY);
 if (distance1l < player1.sizeY + lillepenge1.sizeY && distance1l < player1.sizeX + lillepenge1.sizeX) {
  lillepenge1.removed(); 
  player1saldo = player1saldo + 25000;
  }
 //Sammenstødet mellem player 1 og MellemPenge
 let distance1m;
 distance1m = dist(player1.posX, player1.posY, mellempenge1.posX, mellempenge1.posY);
 if (distance1m < player1.sizeY + mellempenge1.sizeY && distance1m < player1.sizeX + mellempenge1.sizeX) {
  mellempenge1.removed(); 
  player1saldo = player1saldo + 50000;
 }
 //Sammenstødet mellem player 1 og StorPenge
 let distance1s;
 distance1s = dist(player1.posX, player1.posY, storpenge1.posX, storpenge1.posY);
 if (distance1s < player1.sizeY + storpenge1.sizeY && distance1s < player1.sizeX + storpenge1.sizeX) {
  storpenge1.removed();
  player1saldo = player1saldo + 100000;
 }
}

function collisionCheck2() {
 //Sammenstødet mellem player 2 og LillePenge
 let distance2l;
 distance2l = dist(player2.posX, player2.posY, lillepenge2.posX, lillepenge2.posY);
 if (distance2l < player2.sizeY + lillepenge2.sizeY && distance2l < player2.sizeX + lillepenge2.sizeX) {
  lillepenge2.removed();
  player2saldo = player2saldo + 25000;
 }

 //Sammenstødet mellem player 2 og MellemPenge
 let distance2m;
 distance2m = dist(player2.posX, player2.posY, mellempenge2.posX, mellempenge2.posY);
 if (distance2m < player2.sizeY + mellempenge2.sizeY && distance2m < player2.sizeX + mellempenge2.sizeX) {
  mellempenge2.removed(); 
  player2saldo = player2saldo + 50000;
 }

 //Sammenstødet mellem player 2 og StorPenge
 let distance2s;
 distance2s = dist(player2.posX, player2.posY, storpenge2.posX, storpenge2.posY);
 if (distance2s < player2.sizeY + storpenge2.sizeY && distance2s < player2.sizeX + storpenge2.sizeX) {
  storpenge2.removed();
  player2saldo = player2saldo + 100000;
 }
}
