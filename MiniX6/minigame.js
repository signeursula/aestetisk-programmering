let nul = 0;
let player1;
let player2;
let light1;
let light2;
let pinkScore = 0;
let grønScore = 0;
let pinkVinderAlpha = 0;
let grønVinderAlpha = 0;

function setup() {
    createCanvas(950, 600);
    background(255);

    //Classes
    player1 = new Player1();
    player2 = new Player2();
    light1 = new Light1();
    light2 = new Light2();
}

function draw() {
    rectMode(CORNER);

    //Lyserød baggrund
    noStroke();
    fill(255, 125, 194);
    rect(0, 0, 950 / 2, 600);

    //Grøn baggrund
    noStroke();
    fill(99, 245, 95);
    rect(950 / 2, 0, 950 / 2, 600);

    //Første hvide kasse
    fill(255);
    stroke(225, 75, 155);
    strokeWeight(6);
    rect(40, 100, 400);

    //Anden hvide kasse
    fill(255);
    stroke(35, 168, 32);
    strokeWeight(6);
    rect(510, 100, 400);

    //Overskrift: GET THE LIGHT
    fill(60);
    stroke(255);
    strokeWeight(8);
    textFont("Arial Black");
    textSize(60);
    text("GET THE LIGHT", 232, 78);

    //Pink score
    fill(225, 75, 155);
    noStroke();
    textSize(20);
    text("SCORE:", 40, 84);
    text(pinkScore, 130, 84);

    //Grøn score
    fill(35, 168, 32);
    noStroke();
    textSize(20);
    text("SCORE:", 800, 84);
    text(grønScore, 890, 84);

    //Visning af classes
    player1.show();
    player2.show();
    light1.show();
    light2.show();

    //Sammenstødet mellem player1 og light1
    let distance1;
    distance1 = dist(player1.posX, player1.posY, light1.posX, light1.posY);
    if (distance1 < player1.size / 2 + light1.size / 2) {
        pinkScore = pinkScore + 1;
        light1.posX = (random(60, 420));
        light1.posY = (random(120, 480));
    }

    //Sammenstødet mellem player2 og light2
    let distance2;
    distance2 = dist(player2.posX, player2.posY, light2.posX, light2.posY);
    if (distance2 < player2.size / 2 + light2.size / 2) {
        grønScore = grønScore + 1;
        light2.posX = (random(530, 890));
        light2.posY = (random(120, 480));
    }

    //WASD tasterne
    fill(225, 75, 155);
    rect(240, 530, 30);
    rect(240, 570, 30);
    rect(200, 570, 30);
    rect(280, 570, 30);

    fill(255);
    textSize(16);
    text("W", 232, 536);
    text("S", 234, 576);
    text("A", 194, 576);
    text("D", 274, 576);

    //Piletasterne
    fill(35, 168, 32);
    rect(720, 530, 30);
    rect(720, 570, 30);
    rect(680, 570, 30);
    rect(760, 570, 30);

    fill(255);
    textSize(20);
    text("⬆", 710, 538);
    text("⬇", 710, 578);
    text("⬅", 670, 578);
    text("⮕", 750, 578);

    //Pink hvid-brødtekst
    fill(255);
    textSize(10);
    textFont('Helvetica');
    text("Be the first player", 40, 520);
    text("to get 10 lights", 40, 533);
    text("TEAM PINK", 382, 520);

    //Grøn hvid-brødtekst
    fill(255);
    textSize(10);
    textFont('Helvetica');
    text("Be the first player", 830, 520);
    text("to get 10 lights", 843, 533);
    text("TEAM GREEN", 512, 520);

    //Pink vinder
    rectMode(CORNER);
    textFont("Arial Black");
    fill(225, 75, 155, pinkVinderAlpha);
    rect(0, 0, 950, 600);
    textSize(100);
    fill(255, 125, 194, pinkVinderAlpha);
    rect(50, 50, 850, 500);
    fill(255, pinkVinderAlpha);
    text("TEAM PINK", 140, 300);
    textSize(80);
    text("WON!!!", 300, 390)

    //Pink vinder: retry knap
    fill(225, 75, 155, pinkVinderAlpha);
    rect(70, 490, 120, 40);
    fill(255, pinkVinderAlpha);
    textSize(20);
    text("RETRY", 82, 518);
    textSize(26);
    text("↻", 162, 518)

    if (mouseX > 70 && mouseX < 190 && mouseY > 490 && mouseY < 530) {
        stroke(255, pinkVinderAlpha);
        strokeWeight(4);
        fill(225, 75, 155, pinkVinderAlpha);
        rect(70, 490, 120, 40);
        noStroke();
        fill(255, pinkVinderAlpha);
        textSize(20);
        text("RETRY", 82, 518);
        textSize(26);
        text("↻", 162, 518)
    }


    //Grøn vinder
    fill(35, 168, 32, grønVinderAlpha);
    rect(0, 0, 950, 600);
    textSize(100);
    fill(99, 245, 95, grønVinderAlpha);
    rect(50, 50, 850, 500);
    fill(255, grønVinderAlpha);
    text("TEAM GREEN", 106, 300);
    textSize(80);
    text("WON!!!", 300, 390)

    //Grøn vinder: retry knap
    fill(35, 168, 32, grønVinderAlpha);
    rect(70, 490, 120, 40);
    fill(255, grønVinderAlpha);
    textSize(20);
    text("RETRY", 82, 518);
    textSize(26);
    text("↻", 162, 518)

    if (mouseX > 70 && mouseX < 190 && mouseY > 490 && mouseY < 530) {
        stroke(255, grønVinderAlpha);
        strokeWeight(4);
        fill(35, 168, 32, grønVinderAlpha);
        rect(70, 490, 120, 40);
        noStroke();
        fill(255, grønVinderAlpha);
        textSize(20);
        text("RETRY", 82, 518);
        textSize(26);
        text("↻", 162, 518)
    }

    //Hvis team pink får 10 point vis pink vinderside
    if (pinkScore === 10) {
        pinkVinderAlpha = pinkVinderAlpha + 255;
    }

    //Hvis team grøn får 10 point vis grøn vinderside
    if (grønScore === 10) {
        grønVinderAlpha = grønVinderAlpha + 255;
    }
}

function keyPressed() {
    //Player1 bevægelse
    if (keyCode === 87) {
        player1.moveUp();
    }

    if (keyCode === 83) {
        player1.moveDown();
    }

    if (keyCode === 68) {
        player1.moveRight();
    }

    if (keyCode === 65) {
        player1.moveLeft();
    }

    //Player2 bevægelse
    if (keyCode === UP_ARROW) {
        player2.moveUp();
    }

    if (keyCode === DOWN_ARROW) {
        player2.moveDown();
    }

    if (keyCode === RIGHT_ARROW) {
        player2.moveRight();
    }

    if (keyCode === LEFT_ARROW) {
        player2.moveLeft();
    }
}

//Hvis der trykkes retry begyndes resettes spillet
function mouseClicked() {
    if (mouseX > 70 && mouseX < 190 && mouseY > 490 && mouseY < 530) {
        pinkVinderAlpha = nul;
        grønVinderAlpha = nul;
        pinkScore = nul;
        grønScore = nul;
        player1.posX = 240;
        player1.posY = 460;
        light1.posX = 240;
        light1.posY = 300;
        player2.posX = 720;
        player2.posY = 460;
        light2.posX = 710;
        light2.posY = 300;
    }
}