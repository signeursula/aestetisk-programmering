class Light1 {

    constructor() {
        this.posX = 240;
        this.posY = 300;
        this.size = 40;
    }

    show() {
        rectMode(CENTER);
        fill(232, 228, 0, 50);
        ellipse(this.posX, this.posY, this.size);
        fill(232, 228, 0, 150);
        ellipse(this.posX, this.posY, this.size - 10);
        fill(232, 228, 0, 200);
        ellipse(this.posX, this.posY, this.size - 20);
    }
}