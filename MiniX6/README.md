# MiniX6 - README

Link til MiniX6 [https://signeursula.gitlab.io/aestetisk-programmering/MiniX6/index.html]

Link til source code [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX6/minigame.js]

Mit spil hedder "Get the Light" og er et meget simpelt spil, som man kan spille med en ven. Når man åbner programmet, ser man, at kanvasset er splittet i to, således at brugeren får et førstehåndsindtryk af, at spillet er et 1v1 spil. Farverne pink og grøn er valgt som tema, da det er komplimentærfarver, og dermed skaber en følelse af to forskellige hold. Brugerne har hvert deres lille kanvas, altså deres eget spil (uafhængigt af den anden brugers spil). Som titlen hentyder til _get the light_, går spillet ud på, at man fange lyset -altså den lille gule lyskugle i midten af eget kanvas. Selve brugerne styrer hver deres glade lille firkant med enten WASD eller piletasterne, hvilket også står skrevet nedenunder for ekstra brugervenlighed. Spillet starter ved at man styrer sin glade firkant op og rører lyskuglen. Når lyskuglen røres, forsvinder den, der tilføjes et point til scoren (som kan ses ovenstående spilet), og en ny lyskugle opstår et random sted inden for eget kanvas. Det gælder så om at blive den første, der får fanget 10 lyskugler. Vinder team pink, bliver hele skærmen pink med teksten "Team pink won!!!". Modsat hvis team grøn vinder, bliver hele skærmen grøn med teksten "Team green won!!!". Meget simpelt. Jeg har også implimenteret en "Retry ↻" knap, så at man bare kan trykke på den, og ikke skal til at refreshe siden.

**Nu til lidt kode-snak**

Jeg har i alt lavet fire classes. `Player1` som skal fange `Light1` og `Player2` som skal fange `Light2`. Playerne har jeg tegnet ved brug af forskellige `rect();` og lysene har jeg tegnet ved brug af forskellige størrelse `ellipse();` former. Selve lyskuglerne er det eneste runde objekt i hele spillet, da jeg gerne tydeligt ville vise, at det var lyskuglerne, der er "specielle". De glade firkanter har fået ansigt, da jeg gerne ville gøre dem mere identificerbare med brugerne (da det er dem der styrer de glade firkanter).

For at tælle hvor mange lys team pink og team grøn har fået, har jeg startet med at lave variablerne `let pinkScore = 0;` og `let grønScore = 0;`. For at få scoren til at stige, når en lyskugle tages, har jeg skrevet:

```
//Sammenstødet mellem player1 og light1

let distance1;

distance1 = dist(player1.posX, player1.posY, light1.posX, light1.posY);

if (distance1 < player1.size / 2 + light1.size / 2) {

pinkScore = pinkScore + 1;
```

og

```
//Sammenstødet mellem player2 og light2

let distance2;

distance2 = dist(player2.posX, player2.posY, light2.posX, light2.posY);

if (distance2 < player2.size / 2 + light2.size / 2) {

grønScore = grønScore + 1;
```

Når så at en af brugerne opnår en score på 10, sker der følgende: `if (pinkScore === 10) {pinkVinderAlpha = pinkVinderAlpha + 255;`. Lad mig lige forklare dette! Når eksempelvis team pink vinder (ved at fange 10 lyskugler), så bliver hele skærmen pink, og der står "team pink won". Faktisk så er denne "vinder-side" hele tiden til stede. Jeg har bare valgt at gøre den gemmesigtig. Hvilket kan ses her: 

```
//Pink vinder

fill(225, 75, 155, pinkVinderAlpha);

rect(0, 0, 950, 600);

fill(255, 125, 194, pinkVinderAlpha);

rect(50, 50, 850, 500);

fill(255, pinkVinderAlpha);

text("TEAM PINK", 140, 300);

text("WON!!!", 300, 390)
```

Her bør det bemærkes, at jeg har brugt en selvdefineret variabel som alpha-værdi. Den variabel definerede jeg allerede allerførst i min kode: `let pinkVinderAlpha = 0;`. Når så at brugeren får 10 point, så plusses denne selvdefinerede alphaværdi med 255, og vinder-siden bliver derfor synliggjort. Når der så trykkes på retry-knappen, så sættes den selvdefinerede alphaværdi tilbage til 0. Derudover bliver scoren også sat tilbage til 0, og playerne og lysene bliver sat tilbage til deres start position. Hvilket kan ses her:

```
//Hvis der trykkes retry begyndes resettes spillet

function mouseClicked() {

if (mouseX > 70 && mouseX < 190 && mouseY > 490 && mouseY < 530) {

pinkVinderAlpha = nul;

grønVinderAlpha = nul;

pinkScore = nul;

grønScore = nul;

player1.posX = 240;

player1.posY = 460;

light1.posX = 240;

light1.posY = 300;

player2.posX = 720;

player2.posY = 460;

light2.posX = 710;

light2.posY = 300;

}
```

**Fire fejl jeg vil påpege**

Skal jeg være ærlig, så er jeg faktisk meget stolt af mit spil, og jeg synes selv, at det er blevet rigtig godt. Dog vil jeg også gerne påpege nogle af mine egne fejl:

1. Du kan styre din spiller ud af egen spilleboks og over på modstanderens banehalvdel. Du kan dog ikke tage deres lyskugler, så man får ikke så meget ud af det.
2. Du kan også styre din spiller ud af det store kanvas -og så er spilleren er helt på afveje.
3. Når du vinder spillet, er der en retry-knap. Denne knap genstarter spillet. Men når du er i gang med spillet, kan du faktisk godt stadig klikke på retry-knappen og genstarte spillet, da knappen stadig er der, men bare er usynlig.
4. Afhængigt af hvilken computer/laptop du spiller på, så kan skriften godt se anderledes ud, og det gør, at piletasterne og WASD har en forkert placering.

**Lidt reflektioner**

Efterfølgende har jeg gjort mig lidt reflektioner omkring spillet. Er det overhovedet sjovt at hver spiller har hvert sit spil? Havde det været mere intenst, hvis spillerne spillede i _samme_ kanvas om at nå først til _samme_ lyskugle? En anden ting, som jeg også senere hen har tænkt over er, at det måske også kunne have været ret fedt, hvis man bare kunne have holdt piletasterne nede, og så at spilleren havde rykket sig automatisk frem, så at man ikke skulle sidde og trykke klik-klik-klik-klik... Men alt i alt, så er jeg meget tilfreds med mit spil. Herunder kan du se min allerførste skitse, og så nogle screenshot af det endelige program :D

![](MiniX6/minigame_skitse.png)
![](MiniX6/minigame_screenshot1.png)
![](MiniX6/minigame_screenshot2.png)
![](MiniX6/minigame_screenshot3.png)
