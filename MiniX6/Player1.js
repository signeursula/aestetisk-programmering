class Player1 {
  constructor() {
    this.posX = 240
    this.posY = 460;
    this.speed = 20;
    this.size = 50;
  }

  //Styling af player1
  show() {
    //Firkant
    rectMode(CENTER);
    noStroke();
    fill(60);
    rect(this.posX, this.posY, this.size);

    //Firkantens ansigt
    fill(255);
    rect(this.posX, this.posY + 12, this.size - 16, 6);
    rect(this.posX - 14, this.posY + 6, 6, 7);
    rect(this.posX + 14, this.posY + 6, 6, 7);
    rect(this.posX - 13, this.posY - 10, this.size - 42, 12);
    rect(this.posX + 12, this.posY - 10, this.size - 42, 12);
  }

  moveUp() {
    this.posY -= this.speed;
  }

  moveDown() {
    this.posY += this.speed;
  }

  moveRight() {
    this.posX += this.speed;
  }

  moveLeft() {
    this.posX -= this.speed;
  }

}