# MiniX8 - README

**Link til gruppe flowchart 1:** 

[https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX8/Voldtægtsmand_spillet_flowchart.png]


**Link til gruppe flowchart 2:**

[https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX8/Interresse_hjemmeside_flowchart.png]

**Link til selvstændig flowchart:**

[https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX8/Flowchart_over_miniX7.png]

# Gruppe-flowcharts

Gruppe: [Joan](https://gitlab.com/joan6375305/aestetisk-programmering/-/tree/main), [Marius](https://gitlab.com/elmose-gruppe/aestetisk-programmering/-/tree/main/), [Charlotte](https://gitlab.com/charlotteoester/aestetisk-programmering/-/tree/main)

Vi har lavet to flowcharts -inklusiv en selvstændig flowchart. De to flowcharts er idéer til den endelige MiniX11. Vi ville gerne have, at vores idé indeholdte et politisk aspekt, og derfor potentielt kan sætte nogle debatter i gang. 

Vores første idé drager inspiration fra en UNICEF reklame, hvor der bliver fortalt, at afrikanske piger ofte bliver udsat for voldtægt. Dette er jo et tragisk emne, og vi ville godt lave et projekt som ville få folk til at handle og reflektere over, hvad der egentlig foregår rundt i verden. Vi kom på idéen om at lave et spil, hvor man styrer en afrikansk pige, som skal nå hjem og undgå voldtægtsmænd. Lidt alla ‘alle mine kyllinger kom hjem’. Vi tænkte så videre, om hvorvidt man kunne give spillet mere dimension ved at lave multiplayer. Vi tænkte derfra at tage inspiration fra South Park spillet, hvor sværhedsgraden er baseret på hudfarve. Dette er super racistisk, men det siger også meget om det virkelige samfund, hvor kvinder i f.eks Afrika i højere grad bliver ofre for voldtægt, end kvinder i Skandinavien. Vi tænkte på at have to spil (split screen) kørende samtidig på én computer, hvor det så er random om man er en pige fra Afrika eller en pige fra Nordeuropa. Man vinder ved at manøvrere sig hurtigt til målet, sit hjem, uden at støde på en voldtægtsmand. Kvinden på Afrika-siden vil opleve, at der er flere fjender (voldtægtsmænd) i deres spil, modsat den hvide pige. Dette kan virke absurd, men siger også noget om, hvordan kvinder fra samtlige kulturer er ofre for voldtægt, men at kvinder i afrikanske lande i højere grad er udsatte. 

Vores andet projekt handler om, hvordan at dataindsamling kan føre til, at man ender i en indelukket bobbel, hvor man kun bliver fodret med mere og mere af samme holdning, og derfor kan ende i ekstremistiske grupper og selv få ekstremistiske holdninger grundet manglende imput fra folk med andre holdninger end din egen. Dette projekt skulle helt klart have til formål at være sjovt, samtidig med at kunne sætte nogle tanker i gang. Programmet fungerer således, at man starter på en "hjemmeside", som ligner et umiddelbar hub for fritidsinteresser i Århus. Der er så fem muligheder, hvorledes dén man vælger fører til ekstremistiske pop-up reklamer og anbefalede ekstremistiske grupper. 

Når det kommer til flowcharts, var det klart lettest at lave vores første idé, da den er mere simpel. Der er kun to veje, der deler sig i spillet om manøvren i et overdrevent voldtægts præget miljø, med den afrikanske pige og den nordeuropæiske pige. Vores anden idé med ekstremistiske pop-ups var sværere at få formidlet i en flowchart, da der var fem forskellige muligheder. Dette gjorde det rent tegneteknisk svært med, hvordan at man lige skulle koble stregerne imellem de forskellige bokse.

Ved begge flowchart prøvede vi også at simplificere det. Vi bestræbede os efter eksempelvis at kunne give vores flowchart til nogen, som aldrig har hørt om idéen før, men stadig ville kunne forstå idéen ud fra skitsen.

Flowchartene er brugbar i den kontekst, at det giver et meget klart og simpelt overblik over, hvad projekterne præcis går ud på. Havde man bare læst en tekst (ligesom denne readme), kan det godt blive lidt uklart, hvordan programmet egentlig fungerer. Ved at lave et flowchart, kan man se projektets opbygning. Dog kan det også være meget tid at bruge, hvis vi ikke ender med at lave en idé som overhovedet har noget at gøre med disse to.

# Individuel-flowchart

Mit individuelle flowchart viser hvordan min MiniX7 er opbygget. Den skal læses fra toppen og ned, og man starter ved boksen "start". Derefter vises der en boks, hvor der står, at kanvasset vises. De tre kasser som diagrammet forgrener sig ud i, viser kodens classes, henholdsvis "hahaha", "oh my god" og "no way". Diagrammets romber, viser interaktionen med classes'ene som knap. Hvis de klikkes på, føres man videre igennem flowchartet til visning af webcam og tekst (teksten er baseret på den valgte knap). Hvis der ikke trykkes, sker der ingenting. Selvom at der er tre forgreninger, så viser flowcharten simpelt, at det stort set ender med samme resultat. Dette er især også smart ved brugen af flowcharts, da man tydeligt kan få et groft overblik over selve programmet og koden dertil, uden at gå i dybden med specifikke syntakser.


![](MiniX8/Voldtægtsmand_spillet_flowchart.png)
![](MiniX8/Interresse_hjemmeside_flowchart.png)
![](MiniX8/Flowchart_over_miniX7.png)
