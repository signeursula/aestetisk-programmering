 function setup() {
  // put setup code here
  createCanvas(750, 450);
  background (243, 243, 243);

 console.log("Hello Emoji World!");
 }

 function draw() {
 // put drawing code here
 
 console.log(mouseX, mouseY);
 
 noStroke();

 //Emojiernes knap baggrund
 fill(134, 85, 28); 
 cirkel1 = ellipse(200, 200, 330);
 cirkel2 = ellipse(550, 200, 330);
 
 //Emojiens gule baggrund
 fill(234, 153, 55); 
 ellipse(200, 200, 300);
 fill(237, 175, 66);
 ellipse(200, 200, 280);
 fill(254, 211, 82); 
 ellipse(200, 200, 270);

 //Skygge bagved øjne
 fill(237, 175, 66);
 rect(90, 140, 220, 80, 50);

 //Skygge under smøg
 fill(207, 130, 37);
 quad(200, 268, 268, 283, 280, 276, 202, 252);

 //Emojiens venstre øje: baggrund
 fill(134, 85, 28);
 rect(110, 160, 80, 40, 30);
 ellipse(130, 185, 50);
 quad(133, 210, 174, 200, 156, 182, 127, 185);

 //Emojiens venstre øje: øjenkugle
 fill(255, 116, 61)
 ellipse(156, 172, 38);

 //Emojiens venstre øje: øjenlåg
 fill(237, 175, 66);
 rect(140, 146, 40, 15);
 fill(134, 85, 28);
 rect(136, 160, 40, 6, 30);

 //Emojiens højre øje: baggrund
 fill(134, 85, 28);
 rect(210, 160, 80, 40, 30);
 ellipse(270, 185, 50);
 quad(223, 199, 268, 210, 268, 192, 233, 175);

 //Emojiens højre øje: øjenkugle
 fill(255, 116, 61);
 ellipse(244, 172, 38);

 //Emojiens højre øje: øjenlåg
 fill(237, 175, 66);
 rect(230, 146, 40, 15);
 fill(134, 85, 28);
 rect(224, 160, 50, 6, 30);

 //Emojiens skygger under øjenbryn
 fill(207, 130, 37); 
 quad(160, 160, 179, 141, 132, 145, 125, 160);
 quad(274, 160, 268, 141, 220, 145, 240, 160);

 //Emojiens venstre øjenbryn
 fill(134, 85, 28);
 rect(125, 135, 60, 10, 30);
 quad(97, 158, 128, 147, 182, 135, 128, 134);

 //Emojiens højre øjenbryn
 fill(134, 85, 28);
 rect(215, 135, 60, 10, 30);
 quad(214, 144, 266, 145, 300, 153, 273, 135);

 //Skygge ved mund
 fill(207, 130, 37);
 rect(162, 243, 84, 25, 50);

 //Emojiens mund
 fill(134, 85, 28);
 rect(165, 244, 80, 15, 50);

 //Emojiens smøg
 fill(219, 189, 154);
 quad(197, 262, 279, 280, 280, 260, 202, 241);
 ellipse(203, 252, 23);
 fill(247, 225, 200)
 quad(193, 250, 270, 269, 273, 263, 195, 244);

 //Smøgens flamme
 fill(255, 116, 61);
 ellipse(274, 269, 23);
 fill(255, 69, 36);
 ellipse(274, 269, 18);

 //Hvidt skær i øjnene
 fill(255, 255, 255);
 ellipse(143, 172, 12);
 ellipse(257, 172, 12);

 //NU
 //TIL
 //EMOJI
 //NUMMER
 //TO

 //Emojiens gule baggrund
 fill(234, 153, 55); 
 ellipse(550, 200, 300);
 fill(237, 175, 66);
 ellipse(550, 200, 280);
 fill(254, 211, 82); 
 ellipse(550, 200, 270);

 //Skygge over øjnene
 fill(237, 175, 66);
 quad(515, 159, 530, 125, 473, 136, 452, 164);
 quad(585, 160, 565, 125, 623, 137, 640, 164);

 //Emojiens venstre tårer
 fill(36, 134, 202);
 triangle(486, 200, 475, 222, 497, 222);
 ellipse(486, 228, 25);
 fill(92, 179, 225);
 triangle(486, 210, 480, 222, 492, 222);
 ellipse(486, 228, 16);
 
 //Emojiens højre tårer
 fill(36, 134, 202);
 triangle(624, 200, 613, 222, 635, 222);
 ellipse(624, 228, 25);
 fill(92, 179, 225);
 triangle(624, 210, 618, 222, 630, 222);
 ellipse(624, 228, 16);

 //Emojiens venstre øje: baggrund
 fill(134, 85, 28);
 rect(450, 153, 80, 40, 30);
 ellipse(506, 178, 50);
 quad(468, 193, 502, 203, 516, 166, 464, 176);

 //Emojiens venstre øje: øjenkugle
 fill(240, 222, 206);
 ellipse(500, 168, 36);

 //Emojiens venstre øje: øjenlåg
 fill(237, 175, 66);
 rect(470, 140, 40, 15);
 fill(134, 85, 28);
 rect(469, 153, 50, 6, 30);

 //Emojiens højre øje: baggrund
 fill(134, 85, 28);
 rect(568, 153, 80, 40, 30);
 ellipse(591, 178, 50);
 quad(594, 203, 632, 193, 641, 173, 577, 187);

 //Emojiens højre øje: øjenkugle
 fill(240, 222, 206);
 ellipse(598, 168, 36);

 //Emojiens højre øje: øjenlåg
 fill(237, 175, 66);
 rect(580, 140, 36, 15);
 fill(134, 85, 28);
 rect(580, 153, 50, 6, 30);

 //Hvidt skær i øjnene
 fill(255, 255, 255);
 ellipse(493, 166, 12);
 ellipse(607, 166, 12);

 //Emojiens mund
 fill(134, 85, 28);
 arc(550, 270, 120, 40, PI, 0);
 ellipse(495, 268, 10);
 ellipse(605, 268, 10);
 fill(254, 211, 82);
 arc(550, 270, 98, 20, PI, 0);
 fill(237, 175, 66);
 rect(520, 270, 60, 12, 50);

 //Emojiens øjenbryn
 fill(134, 85, 28);
 quad(470, 140, 528, 131, 532, 121, 478, 130);
 quad(569, 131, 625, 140, 616, 131, 564, 121);

 //Emojiens næsebor
 fill(240, 184, 65);
 rect(536, 224, 12, 16, 50);
 rect(552, 224, 12, 16, 50);

 //Tekst
 fill(100);
 textSize(28);
 textFont('Arial Black');

 if (frameCount > 100) {text("H", 40, 410)};
 if (frameCount > 110) {text("E", 62, 410)};
 if (frameCount > 120) {text("R", 82, 410)};

 if (frameCount > 130) {text("E", 120, 410)};
 if (frameCount > 140) {text("R", 140, 410)};

 if (frameCount > 150) {text("T", 176, 410)};
 if (frameCount > 160) {text("O", 195, 410)};

 if (frameCount > 170) {text("A", 234, 410)};
 if (frameCount > 180) {text("N", 256, 410)};
 if (frameCount > 190) {text("D", 278, 410)};
 if (frameCount > 200) {text("R", 299, 410)};
 if (frameCount > 210) {text("E", 321, 410)};

 if (frameCount > 220) {text("E", 360, 410)};
 if (frameCount > 230) {text("M", 380, 410)};
 if (frameCount > 240) {text("O", 406, 410)};
 if (frameCount > 250) {text("J", 428, 410)};
 if (frameCount > 260) {text("I", 445, 410)};
 if (frameCount > 270) {text("'", 454, 410)};
 if (frameCount > 280) {text("S", 461, 410)};

 if (frameCount > 290) {text("!", 490, 410)};
 if (frameCount > 300) {text("!", 500, 410)};
 if (frameCount > 310) {text("!", 510, 410)};

}
