# MiniX2 - README

Link til MiniX2 [https://signeursula.gitlab.io/aestetisk-programmering/MiniX2/index.html]

Link til source code [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX2/MiniX2.js]

**Hvad ser man, og hvad handler programmet om?** 
Når programmet åbnes ses en lysegrå baggrund med to store emojis ved siden af hinanden. Den første emoji har røde øjne og en smøg i munden. Den anden emoji har tårer i øjnene. Under emoji'erne popper der en tekst op, hvor der står "Her er to andre emoji's".

**Hvordan er det lavet? Hvilke (nye/interessante) syntakser bliver brugt?**
Selve projektet blev igen meget statisk, da det er mere et billede/kunstværk frem for noget med interaktion. Der er brugt mange `ellipse()` og `rect()` funktioner. Mange af emoji'ernes ansigtstræk er også en blanding af flere former. F.eks. i det brune omrids af øjnene er brugt følgende:

`//Emojiens venstre øje: baggrund`

`fill(134, 85, 28);`

`rect(110, 160, 80, 40, 30);`

`ellipse(130, 185, 50);`

`quad(133, 210, 174, 200, 156, 182, 127, 185);`

Her er der eksperimenteret med at lave en specifik for ved hjælp af flere former oveni hinanden. Især `quad()` blev rigtig nyttig, da den giver dig mulighed for at lave en firkant ud fra egne koordinater. Det kan blandt andet også ses ved øjenbrynene og smøgen. I starten tog det ret lang tid at finde koordinaterne, da jeg for hvert x- og y-koordinat gik ind i consol.loggen og skrev `mouseX` (og derefter mouseY), for at få den specifikke koordinat for hvert eneste lille led. Dette tog virkelig lang tid. Senere indsatte jeg `console.log(mouseX, mouseY)`, som gjorde, at min consol.log nu kunne fortælle mig den præcise x- og y-værdi for de koordinater jeg skulle bruge. Dette gik væsentligt hurtigere. Selvfølgelig var det stadig en kunstnerrisk tilpasningsprocess, og der blev rettet meget efter øjemål, men jeg vil sige, at det virkelig også har givet mig en god forståelse for koordinater.


**Hvad får projektet mig til at tænke på eller føle?** 
Hvad får dette projekt mig til at føle?... Jeg vil sige, at når jeg ser på emoji'erne -og ved hvor meget arbejde der lå bag dem, så føler jeg mig stolt, da jeg faktisk synes, at det er nogle rimeligt gode bud på et par emojis. Desuden er der også lagt mange tanker i selve farvevalget. Jeg har siddet med RBG-værktøj og taget de præcise farvemål fra de klassiske Apple emojis. Så jeg føler også, at de er meget troværdige -som om de godt kunne være prototyper til rigtige emojis. Selve følelsen emojierne i sig selv bibringer er nok nogle mere ikke-positive følelser. Jeg prøvede at sætte mit fokus på at skabe et udtryk for følelsen af meningsløshed/opgivendhed. Denne følelse synes jeg, at jeg opnår okay godt. Det har også været lidt sjovt at arbejde med følelsen: ikke at føle noget. For det er jo i sig selv en følelse, og så samtidig ikke.

**Lykkeds det med at besvare øvelsen på en fyldesgørende, interessant eller unik måde?**
Umiddelbart vil jeg sige, at det lykkedes rimelig godt med at besvare øvelsen fyldesgørende. Interessant- og unikhedsfaktoren er måske ikke på højeste plan, da det er et meget statisk projekt. Selvfølgelig fik jeg tilføjet noget mere dynamisk tekst, men jeg ville virkelig gerne have haft ydrecirklen (den brune outline af emoji'erne) til at skifte farve, når man hover musemarkøren over den. Det var lidt intentionen, at det skulle ligne, at man kunne klikke på dem. Men den kunne jeg simpelthen ikke finde ud af :( Jeg kunne godt få det til at virke, hvis det havde været en firkant, men mine kodningskompetencer rækte ikke til en cirkel dog. Derfor synes jeg nok, at projektet ikke er så interresant igen. Men selve emoji'erne i sig selv blev meget fede!

**Hvorvidt og hvordan adresserer værket æstetik eller politisk repræsentation via. det konceptuelle eller visuelle?**
Projektet skulle gerne visuelt illustrere følelsen af meningsløshed/opgivenhed, som nævnt tidligere. Måske noget der tilsvarer: 😞😔 -men med endnu mere følelse (eller manglende følelse, om man vil). Det politiske aspekt skulle ligge i, at en smiley kan have svært ved at sætte billede på den præcise følelse, som man går rundt med nede i maven. Og hvordan repræsenterer man så følelsen af ikke at føle noget? Jeg ville gerne lave en emoji, som repræsenter ikke bare at være glad, sur eller trist. Om det er lykkedes til perfektion er en anden sag. Desuden har den ene også en smøg og røde øjne, hvilket på papiret nok ikke er politisk korrekt, da man ikke vil opfordre folk til at ryge. Ikke desto mindre er det stadig normalt, og dét faktum at der ikke er en emoji for det, gør det ikke mindre almindeligt.


![](MiniX2/Emojis.png)
