let fregneTæller = 0;
let nul = 0;

let baggrund;
let outline;
let øjne;
let skygger;
let overskrift;

function setup() {
 console.log("HELLO FRECKLE GENERATOR WORLD!");

 //Her loades billederne
 baggrund = loadImage('baggrund.png');
 outline = loadImage('outline.png');
 øjne = loadImage('øjne.png');
 skygger = loadImage('skygger.png');
 overskrift = loadImage('overskrift.png');

 //Her laves et hvidt kanvas med en hudfarvet firkant (som skal være i baggrunden af ansigtet)
 createCanvas(windowWidth, windowHeight);
 background(255);
 noStroke();
 fill(235, 198, 171);
 rect(0,0,600,600);
}

function draw() {
 
 //Her indsættes billedern
 image(baggrund, 0, 0, 600, 600);
 image(skygger, 0, 0, 600, 600);
 image(øjne, 0, 0, 600, 600);
 image(outline, 0, 0, 600, 600);
 image(overskrift, 500, 120);

 //Her laves en sideløbende tæller synkront med frameCounten
 if(frameCount % 1 == 0){
 fregneTæller = fregneTæller + 1;}

 //Når fregneTælleren rammer 10 tilføjes der en fregne, og fregneTælleren resettes til 0
 if (fregneTæller = 10){
 noStroke();
 fill (163, 93, 42, random(40, 80));
 ellipse(random(108, 480), random(111, 577), random(5, 12));
 fregneTæller = nul;}
}
