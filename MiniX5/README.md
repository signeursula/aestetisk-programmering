# MiniX5 - README

Link til MiniX5 [https://signeursula.gitlab.io/aestetisk-programmering/MiniX5/index.html]

Link til source code [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX5/freckle-generator.js]

Jeg har lavet en fregne-generator kaldet "The Freckle Generator", og som navnet antyder, så genererer mit program fregner! Åbner man programmet, ser man et ansigt. I dét at fanen åbnes begynder fregnerne at genereres. Dette gøres ved, at jeg først har lavet en fregneTæller: `let fregneTæller = 0;`, som løber synkront med frameCounten:

```
if(frameCount % 1 == 0){ 
fregneTæller = fregneTæller + 1;}
```

Derefter laver jeg en "regel" for min generator. Denne regel gør således, at når frameCounten (og fregneTælleren) rammer 10, så tilføjes der en fregne et tilfældigt sted på ansigtet. Jeg har gjort sådan, at fregnen også varierer lidt i størrelse og gennemsigtighed:

```
if (fregneTæller = 10){
noStroke();
fill (163, 93, 42, random(40, 80));
ellipse(random(108, 480), random(111, 577), random(5, 12)); 
fregneTæller = nul;}
```
Til sidst har jeg skrevet `fregneTæller = 0;` da det gør, at fregneTælleren ressettes og igen laver en ny fregne, når den rammer 10 igen. 

Ansigtet har jeg selv tegnet, og jeg har opdelt lagene i forskellige billeder. Selve hudfarven har jeg lavet ved at sætte en beigefarvet firkant ind i `function setup()`. Havde jeg sat firkanten ind i `function draw()`, så havde der konstant været en hudfarvet firkant over fregnerne, og man ville ikke kunne se fregnerne blive lavet.

- Nårh ja, jeg har også selv skrevet overskriften (hvilken man vist godt kan se, men det har også lidt charme med håndskrevet skrift. Desuden er den super meget stavet forkert med vilje, eller noget!!!).

The Freckle Generator har i teorien ikke et sluttidspunkt, da den bliver ved og ved og ved med at generere fregner, men sidder du lang tid nok og venter, vil ansigtet blive så fyldt med fregner, at man ikke længere kan se, at der tilføjes fregner. (Det er lowkey lidt blackfacing, ups). 

![](MiniX5/freckle-generator_screenshot.png)
