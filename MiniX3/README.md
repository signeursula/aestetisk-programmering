# MiniX3 - README

Link til MiniX3 [https://signeursula.gitlab.io/aestetisk-programmering/MiniX3/index.html]

Link til source code [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX3/throbber.js]

**Min throbber**

Min throbber består af fire lilla/pink blomster, som roterer rundt med urets retning. Selve blomsterne roterer også rundt om sig selv. Dette var en større udfordring for mig, en jeg havde troet. Jeg startede med at få de fire blomster til at roterer rundt om midten. Men det var svært at få blomsterne til selv at snurre rundt, da jeg ikke kunne angive et specifikt midterpunk, da blomsterne hele tiden flyttede sig.

Jeg løste det ved at gøre følgende:
 
 `//BLOMST 1`

 `push();`

 `translate(-100, 0);` Her sætter jeg midterpunktet (punktet som der roteres om) til at være blomsten midte.

 `rotate(vinkel);` Her roterer den enkelte blomst så om overstående koordinat.

 `noStroke();`

 `fill(103, 29, 207);` Her laves selve blomsten.

 `ellipse(0, 0, 30, 110);` ...

 `ellipse(0, 0, 110, 30);` ...

 `rotate(45);`

 `ellipse(0, 0, 30, 110);` ...

 `ellipse(0, 0, 110, 30)` ...

 `pop();`

Dette gjorde jeg ved alle fire blomster, hvor de så alle havde forskellige koordinator. Dette gjorde, at jeg nu havde fire stilstående blomster, som roterede rundt uden at flytte sig fra stedet. Til sidst tog jeg så hele kanvasset og roterede rundt. Dette giver en effekt af, at det hele går rigtig hurtigt, og at der er meget bevægelse.

Jeg har også skrevet den klassiske "loading" tekst efterfulgt af "dot dot dot" som popper op. Dette var også en svær process at finde frem til, da jeg ikke før har lavet `if` og `if else` statements.
Først og fremmest skulle jeg også sørge for, at selve "loading" teksten ikke drejede rundt med hele kanvasset. Det fiksede jeg ved at placere teksten øverst i min kode, og først derefter rotere kanvasset.

Min "dot dot dot" blev lavet følgende:

 `//Dot dot dot tekst`

 `if(frameCount % 10 == 0) {`

 `punktum = punktum + 1; }` Jeg har lavet en variabel der hedder "punktum". Den plusses med 1 for hvert tiende framecount.

 `if (punktum == 1) {`

 `text('.', 30, 230); }` Hvis punktum er lig med 1, skal der vises ét punktum.

 `else if (punktum == 2) {`

 `text('.', 40, 230);` 

 `text('.', 30, 230); }` Hvis punktum er lig med 2, skal der vises det første punktum -plus nummer to punktum.

 `else if (punktum == 3) {` 

 `text('.', 50, 230);`

 `text('.', 30, 230);`

 `text('.', 40, 230); }` Hvis punktum er lig med 3, skal der vises det første punktum, nummer to punktum -plus nummer tre punktum.

 `else {punktum = 0}` Hvis punktum hverken er 1, 2 eller 3 (altså højere), skal punktum sættes tilbage til værdien 0.

(Hvis man åbner consol.loggen kan man se hvordan variablen "punktum" virker)

**Hvordan viser mit program tid?**

Min throbber siger noget om tid, i selve bevægelses aspektet. Det hele går meget hurtigt -eller det føles i hvert fald sådan. Dét at blomsterne individuelt drejer rundt, for det hele til at føles meget hurtigere. Tempoet har været det samme, da blomsterne var statiske og bevægede sig rundt om midten -alligevel føles det hurtigere sådan her. Dette siger noget om tid, og det er sjovt at tænke over, hvordan at små rettelser i designet kan ændre på vores opfattelse af, hvor hurtigt noget bevæger sig.

**Throbbers i digital kultur**

Det er først efter at have arbejdet med at kode throbbers, at jeg rigtig har lagt mærke til dem. Dog har throbbers altid vakt følelser i mig. Hvis en throbber kommer inden at et program startes, kan jeg bedre overskue det, og jeg har en opfattelse om, at programmet "tænker". Kommer throbberen bare pludseligt og uventet frem midt i en handling/streaming, bliver jeg meget mere frustreret. F.eks. hvis jeg ser en film eller spiller et spil. Dette vækker nærmest en vrede i mig. Jeg tror også, efter at have tænkt over det, at jeg foretrækker throbbers med tekst. F.eks. når der står "loading" ligesom jeg selv har skrevet i min. Jeg tror, at det bunder ud i en følelse af mistillid til, om der oprigtig loades. Hvilket er ironisk, for selvfølgelig gør der det. Men lige så snart, at der er en tekst, så bliver jeg lidt mere rolig omkring det. Jeg føler også, at når der også er en tekst, så er det som om, at throbberen kommunikerer lidt mere med mig.

![](MiniX3/Throbber_screenshot.png)

Her er også et billede af min skitse (hvis du nu skulle være interreseret)

![](MiniX3/Throbber_skitse.png)
