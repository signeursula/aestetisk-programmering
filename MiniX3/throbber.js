let vinkel = 0;
let punktum = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(230);
  frameRate(30);
  angleMode(DEGREES);
}

function draw() {
  console.log(punktum);
  translate(width/2, height/2);
  background(230);

 //Loading tekst
 textAlign(CENTER);
 textFont('Arial Black');
 textSize(20);
 fill(70);
 text('Loading', -20, 230);

 //Dot dot dot tekst
 if(frameCount % 10 == 0){
 punktum = punktum + 1;
 }

 if (punktum == 1) {
 text('.', 30, 230); }
 else if (punktum == 2) {
 text('.', 40, 230);
 text('.', 30, 230); }
 else if (punktum == 3) {
 text('.', 50, 230); 
 text('.', 30, 230);
 text('.', 40, 230); }
 else {punktum = 0}

  rotate(vinkel);
  vinkel += 1;

 //BLOMST 1
 push();
   translate(-100, 0);
   rotate(vinkel); 
   noStroke();
   fill(103, 29, 207);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
   rotate(45);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
    
     //Hvid gennemsigtig
     fill(255, 50);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
     rotate(45);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
   vinkel += 1;
 pop();

 //BLOMST 2
 push();
   translate(100, 0);  
   rotate(vinkel); 
   noStroke();
   fill(163, 29, 207);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
   rotate(45);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
     
     //Hvid gennemsigtig
     fill(255,50);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
     rotate(45);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
   vinkel += 1;
 pop();

 //BLOMST 3
 push();
   translate(0, 100);    
   rotate(vinkel); 
   noStroke();
   fill(207, 29, 201);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
   rotate(45);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
     
     //Hvid gennemsigtig
     fill(255,50);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
     rotate(45);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
   vinkel += 1;
 pop();

 //BLOMST 4
 push();
   translate(0, -100);   
   rotate(vinkel); 
   noStroke();
   fill(222, 51, 148);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
   rotate(45);
   ellipse(0, 0, 30, 110);
   ellipse(0, 0, 110, 30);
    
     //Hvid gennemsigtig
     fill(255, 50);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
     rotate(45);
     ellipse(0, 0, 20, 100);
     ellipse(0, 0, 100, 20);
   vinkel += 1;
 pop();

}