let knap;
let musik;
let musikHastighed = 0;

let vers1;
let vers2;

let sentence1;
let sentence2;
let sentence3;
let sentence4;
let sentece5;
let sentence6;
let sentence7;
let sentece8;

function preload() {
  //Her loades JSON filerne
  vers1 = loadJSON("vers1.json");
  vers2 = loadJSON("vers2.json");

  //Her loades musikken
  musik = loadSound('musik.mp3');
  musik.setVolume(0.3);
}

function setup() {  
  //Her laves kanvasset og teksten tilpasset
  createCanvas(1280, 550);
  textSize(16);
  fill(255);
  textAlign(CENTER, CENTER);

  //Her er alle de randomgenerede ord fra vers1 (json filen)
  //Math.floor runder ned til nærmeste hele tal
  //Math.random vælger et tal mellen 0-0.99999999
  const randomA1 = vers1.a1[Math.floor(Math.random() * vers1.a1.length)];
  const randomA2 = vers1.a2[Math.floor(Math.random() * vers1.a2.length)];
  const randomA3 = vers1.a3[Math.floor(Math.random() * vers1.a3.length)];
  const randomA4 = vers1.a4[Math.floor(Math.random() * vers1.a4.length)];
  const randomA5 = vers1.a5[Math.floor(Math.random() * vers1.a5.length)];
  const randomA6 = vers1.a6[Math.floor(Math.random() * vers1.a6.length)];
  const randomA7 = vers1.a7[Math.floor(Math.random() * vers1.a7.length)];
  const randomA8 = vers1.a8[Math.floor(Math.random() * vers1.a8.length)];
 
  //Her er alle de randomgenerede ord fra vers2 (json filen)
  const randomA9  = vers2.a9 [Math.floor(Math.random() * vers2.a9.length)];
  const randomA10 = vers2.a10[Math.floor(Math.random() * vers2.a10.length)];
  const randomA11 = vers2.a11[Math.floor(Math.random() * vers2.a11.length)];
  const randomA12 = vers2.a12[Math.floor(Math.random() * vers2.a12.length)];
  const randomA13 = vers2.a13[Math.floor(Math.random() * vers2.a13.length)];
  const randomA14 = vers2.a14[Math.floor(Math.random() * vers2.a14.length)];
  const randomA15 = vers2.a15[Math.floor(Math.random() * vers2.a15.length)];

  //Her bliver vers 1 sammensat af fire sætninger
  sentence1 = "I " + randomA1.ord + " stiger " + randomA2.ord + " op";
  sentence2 = "Den spreder " + randomA3.ord + " på " + randomA4.ord;
  sentence3 = "Går over " + randomA5.ord + " og " + randomA6.ord;
  sentence4 = "Går over " + randomA7.ord + " og " + randomA8.ord;

  //Her bliver vers 2 sammensat af fire sætninger
  sentence5 = "Den kommer fra den favre " + randomA9.ord; 
  sentence6 = " Hvor " + randomA10.ord + " lå ";
  sentence7 = "Den bringer " + randomA11.ord + " og " + randomA12.ord + " og " + randomA13.ord; 
  sentence8 = " Til " + randomA14.ord + " og til " + randomA15.ord;

  //Her designes vores "Mere poesi" knap"
  knap = createButton ('Mere poesi');
  knap.position(width/2-50, height/2+100)
  knap.size(100, 30);
  knap.style('background-color','#000000');
  knap.style('color','#FFFFFF');

  knap.mousePressed(refreshDigt);
}

function draw() {
  //Musik hastighed
  musik.rate(musikHastighed);
  
  //Her laves baggrunden
  background(0);
  const x = width / 2;
  const y = height / 2 - 100;
  
  //Her vises al teksten på kanvasset
  fill(255);
  textSize(16);
  textFont('Georgia'); 
  textStyle(NORMAL);
  text(sentence1, x, y);
  text(sentence2, x, y+20);
  text(sentence3, x, y+40);
  text(sentence4, x, y+60);
  text(sentence5, x, y+100);
  text(sentence6, x, y+120);
  text(sentence7, x, y+140);
  text(sentence8, x, y+160);

  //Her vises overskriften
  textSize(30);
  textStyle(BOLD);
  textFont('Georgia');
  text(sentence1, x, y-40); 
}

function refreshDigt () {
 //Her er alle de randomgenerede ord fra vers1 (json filen)
 const randomA1 = vers1.a1[Math.floor(Math.random() * vers1.a1.length)];
 const randomA2 = vers1.a2[Math.floor(Math.random() * vers1.a2.length)];
 const randomA3 = vers1.a3[Math.floor(Math.random() * vers1.a3.length)];
 const randomA4 = vers1.a4[Math.floor(Math.random() * vers1.a4.length)];
 const randomA5 = vers1.a5[Math.floor(Math.random() * vers1.a5.length)];
 const randomA6 = vers1.a6[Math.floor(Math.random() * vers1.a6.length)];
 const randomA7 = vers1.a7[Math.floor(Math.random() * vers1.a7.length)];
 const randomA8 = vers1.a8[Math.floor(Math.random() * vers1.a8.length)];
 //Her er alle de randomgenerede ord fra vers2 (json filen)
 const randomA9  = vers2.a9 [Math.floor(Math.random() * vers2.a9.length)];
 const randomA10 = vers2.a10[Math.floor(Math.random() * vers2.a10.length)];
 const randomA11 = vers2.a11[Math.floor(Math.random() * vers2.a11.length)];
 const randomA12 = vers2.a12[Math.floor(Math.random() * vers2.a12.length)];
 const randomA13 = vers2.a13[Math.floor(Math.random() * vers2.a13.length)];
 const randomA14 = vers2.a14[Math.floor(Math.random() * vers2.a14.length)];
 const randomA15 = vers2.a15[Math.floor(Math.random() * vers2.a15.length)];

 //Her bliver vers 1 sammensat af fire sætninger
 sentence1 = "I " + randomA1.ord + " stiger " + randomA2.ord + " op";
 sentence2 = "Den spreder " + randomA3.ord + " på " + randomA4.ord;
 sentence3 = "Går over " + randomA5.ord + " og " + randomA6.ord;
 sentence4 = "Går over " + randomA7.ord + " og " + randomA8.ord;

 //Her bliver vers 2 sammensat af fire sætninger
 sentence5 = "Den kommer fra den favre " + randomA9.ord; 
 sentence6 = "Hvor " + randomA10.ord + " lå ";
 sentence7 = "Den bringer " + randomA11.ord + " og " + randomA12.ord + " og " + randomA13.ord; 
 sentence8 = "Til " + randomA14.ord + " og til " + randomA15.ord;
 }

 //Musikken starter når der klikkes, og bliver hurtigere for hvert klik
 function mousePressed (){
  musikHastighed = musikHastighed + 1;
  musik.play();
}

