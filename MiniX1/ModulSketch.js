function setup() {
  // put setup code here
createCanvas(500,500);
background (255,133,61);
}

function draw() {
  // Baggrunds sort streg
  fill(255,132,70)
  square(10, 10, 480);
  
  // Solsikkens stilk
  fill(0,0,0);
  rect(235,230,30,300)

  // Solsikkens bagerste blade
  fill(255,198,38);
  ellipse(250,150,120,120)
  ellipse(250,350,120,120)
  ellipse(150,250,120,120)
  ellipse(350,250,120,120)

  // Solsikkens forreste blade
  fill(255,243,68);
  ellipse(315,175,120,120)
  ellipse(175,315,120,120)
  ellipse(315,315,120,120)
  ellipse(175,175,120,120)
  
  // Solsikkens midte
  fill(0,0,0);
  ellipse(250,250,150,150);

  // Solsikkens mund
  fill(255,255,255);
  ellipse(250, 260, 105, 100);
  fill(0,0,0);
  rect(190,210,115,50)
  
  // Solsikkens øjne
  fill(255,255,255);
  ellipse(220,230,30,30);
  ellipse(280,230,30,30);
  fill(0,0,0);
  ellipse(218,220,15,15);
  ellipse(282,220,15,15);

  console.log("Hello Sunflower World!");

}
