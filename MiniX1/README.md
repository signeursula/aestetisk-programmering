# MiniX1 - README

Link til MiniX1 [https://signeursula.gitlab.io/aestetisk-programmering/MiniX1/index.html]

**Hvad har jeg lavet?** Jeg har taget et mere kunstnerisk approach til MiniX1, og ville derfor gerne lave et billede. Det endte ud i at blive en glad solsikke! 

**Hvad betyder kode og programmering for mig**
Hvad programmering betyder for mig?... Umiddelbart synes jeg, at programmering er virkelig fedt! Jeg har ingen erfaring indenfor programmering, men jeg er meget fascineret af kodning. Det giver en bredere forståelse for, hvad der egentlig ligger bag den teknologi, som vi så flittigt benytter os af på daglig basis. Jeg håber på en dag selv at nå et niveau, hvor jeg bliver decent til at kode -eller i hvert fald forstår det, så det giver mening.

**Beskrivelse af det første selvstændige kodningsprojekt:** Jeg har syntes, at det har været sjovt -og lidt udfordrende at kode (også selvom at det her kun er på total begynderniveau). Jeg får allerede mange visioner for, hvad man ellers kunne lave. For eksempel kunne en opgradering til solsikken være at få noget bevægelse ind i framet, så det ikke bare er et statisk billede. Jeg kunne eksempelvis få solsikken til at blinke med øjnene? Man lærer også hurtigt mange ting ved at kode! Især også ved at lave fejl. Jeg fandt blandt andet hurtigt ud af, at det er praktisk at kommentere så meget som muligt -i hvert fald når jeg nu ikke er den bedste til at kode endnu, så kan man hurtig blive forvirret. Derfor hjælper det meget at kommenterer. Det er desuden også sjovt, hvordan man skal tænke i lag. Hvad skal være bagerst, i mellemgrunden og forrest, hvilket for betydning for, hvordan koden skal skrives. Det er dog noget, som jeg føler mig mere stærk i, da jeg har tegnet meget digitalt, og derfor har været vant til at arbejde meget i lag.

**Hvordan er kodningsprocessen lig med eller anderledes fra tekstlæsning- og skrivning?**
Jeg vil sige, at det er meget forskelligt fra at skulle læse/skrive. Jeg har altid været god til at skrive stile, rapporter, diverse tekster, you name it... Men at skrive kode er langt mere teknisk. Der er typisk ingen alternativer -ingen veninder eller forskellige formuleringer. Skulle det sammenlignes med noget, skulle det være at skrive en kogebog. Det er en slags lang opskrift, som skal stå i den rigtige rækkefølge, og som skal være linet op på en bestemt måde. Selvom at jeg vil mene, at kodeskrivning versus tekstskrivning er meget forskelligt fra hinanden, så er jeg alligevel meget begejstret for begge ting på hver deres måde. 


![](MiniX1/Solsikke.png)
