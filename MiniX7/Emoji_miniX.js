let nul = 0;
let capture;

let grine_emoji;
let gisp_emoji;
let græde_emoji;

let spejl;
let ramme_billede;
let spejl_alpha = 255;

let hahaha;
let hahaha_billede;
let hahaha_alpha = 0;
let spejl_hahaha_alpha = 0;

let oh_my_god;
let oh_my_god_billede;
let oh_my_god_alpha = 0;
let spejl_oh_my_god_alpha = 0;

let no_way;
let no_way_billede;
let no_way_alpha = 0;
let spejl_no_way_alpha = 0;

///////////////////////////////////////////////////////////////////

function setup() {
 createCanvas(1280, 660);
 background(216, 225, 240);

 //Classes
 hahaha = new Hahaha();
 oh_my_god = new Oh_my_god();
 no_way = new No_way();
 spejl = new Spejl();

 //Web cam capture
 capture = createCapture(VIDEO);
 capture.size(427, 320);
 capture.hide();

 //Billeder
 hahaha_billede = loadImage('./Images/Hahaha.png');
 no_way_billede = loadImage('./Images/No_way.png');
 oh_my_god_billede = loadImage('./Images/Oh_my_god.png');
 ramme_billede = loadImage('./Images/Ramme.png');
 grine_emoji = loadImage('./Images/grine_emoji.png');
 gisp_emoji = loadImage('./Images/gisp_emoji.png');
 græde_emoji = loadImage('./Images/græde_emoji.png');
}

function draw() {
 background(216, 225, 240);
 
 //Grå marginer i top og bund
 fill(60);
 rect(0, 100, 1280, 60);
 rect(0, 600, 1280, 60);

 //Blå top margin med tekst
 fill(24, 127, 251);
 rect(0, 0, 1280, 100);
 fill(255);
 textSize(56);
 textFont("Arial Black");
 text("SEND EN BESKED MED EN EMOJI", 120, 74);

 hahaha.show();
 oh_my_god.show();
 no_way.show();
 spejl.show();

 //Hvis musen hover over "hahaha"
 if (mouseX > 58 && mouseX < 324 && mouseY > 227 && mouseY < 278) {
 hahaha_alpha = 255;}
 else {hahaha_alpha = nul;}

 //Hvis musen hover over "oh my god"
 if (mouseX > 58 && mouseX < 324 && mouseY > 327 && mouseY < 379) {
 oh_my_god_alpha = 255;}
 else {oh_my_god_alpha = nul;}

 //Hvis musen hover over "no way"
 if (mouseX > 58 && mouseX < 324 && mouseY > 427 && mouseY < 479) {
 no_way_alpha = 255;}
 else {no_way_alpha = nul;}

 //Firkant der dækker for spejlet
 push();
 fill(216, 225, 240, spejl_alpha);
 rect(344, 180, 534, 390);
 pop();

 
//Hvis der trykkes på Hahaha kanppen
push();
fill(45, spejl_hahaha_alpha);
rect(900, 200, 350, 360, 15);
fill(255, spejl_hahaha_alpha);
textSize(18);
textFont("Arial");
text("Umiddelbart ligner du ikke", 1012, 260);
text("en der griner...", 1012, 280); 

textSize(26); 
textFont("Arial Black"); 
text("EMOJI-FACE-READER", 920, 350);
textSize(25); 
text("ANBEFALER I STEDET:", 919, 378);

fill(134, 85, 28, spejl_hahaha_alpha); 
ellipse(1070, 470, 140);
fill(234, 153, 55, spejl_hahaha_alpha); 
ellipse(1070, 470, 132);
fill(254, 211, 82, spejl_hahaha_alpha);
ellipse(1070, 470, 126);
fill(134, 85, 28, spejl_hahaha_alpha);
rect(1030, 480, 80, 10, 100);
ellipse(1050, 450, 20);
ellipse(1090, 450, 20);
pop();

if (spejl_hahaha_alpha === 255) {
image(grine_emoji, 918, 225, 75, 70);}

//Hvis der trykkes på Oh My God kanppen
push();
fill(45, spejl_oh_my_god_alpha);
rect(900, 200, 350, 360, 15);
fill(255, spejl_oh_my_god_alpha);
textSize(18);
textFont("Arial");
text("Umiddelbart ligner du ikke", 1012, 260);
text("en der er overrasket...", 1012, 280); 

textSize(26); 
textFont("Arial Black"); 
text("EMOJI-FACE-READER", 920, 350);
textSize(25); 
text("ANBEFALER I STEDET:", 919, 378);

fill(134, 85, 28, spejl_oh_my_god_alpha); 
ellipse(1070, 470, 140);
fill(234, 153, 55, spejl_oh_my_god_alpha); 
ellipse(1070, 470, 132);
fill(254, 211, 82, spejl_oh_my_god_alpha);
ellipse(1070, 470, 126);
fill(134, 85, 28, spejl_oh_my_god_alpha);
rect(1030, 480, 80, 10, 100);
ellipse(1050, 450, 20);
ellipse(1090, 450, 20);
pop();

if (spejl_oh_my_god_alpha === 255) {
image(gisp_emoji, 920, 225, 70, 75);}


//Hvis der trykkes på No Way kanppen
push();
fill(45, spejl_no_way_alpha);
rect(900, 200, 350, 360, 15);
fill(255, spejl_no_way_alpha);
textSize(18);
textFont("Arial");
text("Umiddelbart ligner du ikke", 1012, 260);
text("en der græder...", 1012, 280); 

textSize(26); 
textFont("Arial Black"); 
text("EMOJI-FACE-READER", 920, 350);
textSize(25); 
text("ANBEFALER I STEDET:", 919, 378);

fill(134, 85, 28, spejl_no_way_alpha); 
ellipse(1070, 470, 140);
fill(234, 153, 55, spejl_no_way_alpha); 
ellipse(1070, 470, 132);
fill(254, 211, 82, spejl_no_way_alpha);
ellipse(1070, 470, 126);
fill(134, 85, 28, spejl_no_way_alpha);
rect(1030, 480, 80, 10, 100);
ellipse(1050, 450, 20);
ellipse(1090, 450, 20);
pop();

if (spejl_no_way_alpha === 255) {
image(græde_emoji, 920, 225, 70, 75);}
}

function mouseClicked(){
 if (hahaha_alpha === 255) {
 spejl_alpha = 0;
 spejl_hahaha_alpha = 255;
 spejl_oh_my_god_alpha = 0;
 spejl_no_way_alpha = 0;
 }

 if (oh_my_god_alpha === 255) {
 spejl_alpha = 0;
 spejl_oh_my_god_alpha = 255;
 spejl_hahaha_alpha = 0;
 spejl_no_way_alpha = 0;
 }

 if (no_way_alpha === 255) {
 spejl_alpha = 0;
 spejl_no_way_alpha = 255;
 spejl_hahaha_alpha = 0;
 spejl_oh_my_god_alpha = 0;
 }
}

