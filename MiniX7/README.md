# MiniX7 - README

Link til MiniX7 [https://signeursula.gitlab.io/aestetisk-programmering/MiniX7/index.html]

Link til source code [https://gitlab.com/signeursula/aestetisk-programmering/-/blob/main/MiniX7/Emoji_miniX.js]

**Beskrivelse**

Mit program er ret enkelt. Når du åbner siden, er der en overskrift, hvor der står "Send en besked med en emoji". Nedenunder er tre valgmuligheder. Disse muligheder er sat op i små talebobler ligesom beskederne på eksempelvis SMS og Messenger. Når du placerer musemarkøren over en af de tre beskeder, kommer der en hvid outline rundt om beskeden. Klikker du så på den besked, som du vælger at "sende", så vil der poppe et spejl op, hvor du kan se dig selv. Ved siden af, vil der være en tekstboks, hvor der står, at du ikke umiddelbart ligner den valgte emoji, og at _Emoji Face Reader_ foreslår en anden emoji. Den foreslåede emoji er så en resting-face emoji (stone-face emoji).

**Hvad har jeg lært?**

Jeg har helt klart fået prøvet krafter af med flere classes -i en situation der ikke er relateret til spil. Her er classes'ene bare en anden måde at lave knapperne på, og så er de derefter lettere at rykke rundt med og justere. Det er klart noget, som jeg gerne vil arbejde mere med. Dog havde jeg svært ved at lave et if-statement, hvor jeg får en hidden class til at blive showet. 

Jeg har helt klart også lært, at det hurtigt kan blive uoverskueligt, når man arbejder med mange billedfiler. Derfor er det vigtigt, at navngive dem rigtigt. Det ved jeg ikke, om jeg fik gjort til perfektion, for jeg fik hurtigt kludret mange af billedfils-navnene sammen. Det vil jeg helt klart tage med mig i baggagen til næste gang.

Her kan billedfilerne ses. Jeg har aldrig før haft så mange variabler i en kode. Også fordi at jeg gerne ville tildele bestemte elementer deres egen alpha-variabel, så jeg senere kunne skrue op og ned for dem.

```
let nul = 0;
let capture;

let grine_emoji;
let gisp_emoji;
let græde_emoji;

let spejl;
let ramme_billede;
let spejl_alpha = 255;

let hahaha;
let hahaha_billede;
let hahaha_alpha = 0;
let spejl_hahaha_alpha = 0;

let oh_my_god;
let oh_my_god_billede;
let oh_my_god_alpha = 0;
let spejl_oh_my_god_alpha = 0;

let no_way;
let no_way_billede;
let no_way_alpha = 0;
let spejl_no_way_alpha = 0;
```

**Min emoji i en større social og kulturel kontekst**

Tanken bag mit projekt er især udsprunget af mit første forsøg på at lave denne opgave (miniX2). Vi havde inden da snakket meget om, om der burde være flere emojier, således at man endnu mere præcist kunne sætte ikon på sine følelser -eller derimod færre emojis, så at emojis er mere overskuelige. Føler folk sig repræsenteret? Bør man altid føle sig repræsenteret i alle situationer?

I min miniX2 lavede jeg to yderligere emojier. De skulle være provokerende i form af at bryde reglerne om, hvad en emoji må symbolisere. Derfor havde jeg blandt andet lavet en som røg. Men jeg har tænkt over det, og måske nytter det ingenting konstant at tilføje flere emojis. Mange emojis er alligevel designet ud fra karikatur, hvor følelserne tegnes endnu mere ekstremt for at give en effekt. Og jeg kom til at tænke på, at ofte når man sender en emoji med i sine sms'er, så ser man slet ikke ud ligesom sin valgte emoji. Man kan sagtens føle ting uden at lave et ansigtsudtryk. Mit projekt er derfor lidt en provokation af selve emoji konceptet. Programmet skal overraske dén som åbner det første gang. De skal sende en besked, og pludseligt se sig selv sidde og "stene" foran computerskærmen. Jeg vil gerne sætte nogle tanker i gang i hovedet på brugeren. Jeg vil gerne vise, at man lidt glemmer, at man ofte slet ikke laver alle mulige ekstreme ansigtsudtryk, så hvorfor så hele tiden tilføje flere emojis?

Det er alt i alt, hvad mit projekt handler om!
Man kunne selvfølgelig også snakke mere inklusivitet ind i det. Hvorfor er emojien gul? Appelerer det ikke mest til hvide mennesker? Men det korte svar er, at jeg i denne omgang holdte mig til de "klassiske" apple emoji farver, og det er mere selve konceptet om ansigtsudtrykket der er bemærkelsesværdigt ved dette projekt.

Tak for at læse med! :De

![](MiniX7/emoji-program_screenshot.png)
